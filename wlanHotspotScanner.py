#!/usr/bin/env python

from subprocess import PIPE, Popen, check_output
import re, time
import cStringIO

cmd = 'sudo iwlist wlan0 scan'
wlanScanFile = '/home/pi/laser_atlas/data/wlanScanFile'

def console(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	out, err= p.communicate()
	return (p.returncode, out, err)

if __name__ == "__main__":
	''' Run command and get the outputs'''

	# Epoch time in second
	timeStamp = int(round(time.time()*1000))

	out = check_output(["sudo", "iwlist", "wlan0", "scan"])

	# if no output message returned
	if len(out) == 0:
		print 'No output from cmd'
		exit()
		
	# parse the output
	strlist = re.split('Cell ', out)
	
	if len(strlist) <= 1:
		print 'No valid wifi hotpot found'
		exit()
	
	scanItem = cStringIO.StringIO()
	scanItem.write('{')
	scanItem.write("time:{},".format(timeStamp))
	print >>scanItem, '['
	
	i = 0
	for cell in strlist:
		tmplist = re.split('Address: ', cell)
		if len(tmplist) <= 1:
			continue
		
		if i > 0:
			print >>scanItem, ','
		print >>scanItem, '{'
		
		address = re.split('\n', tmplist[1])[0]
		print >>scanItem, "Address:"+address+','
		
		tmplist = re.split('Quality=', cell)
		if len(tmplist) <= 1:
			continue
		
		quality = re.split(' ', tmplist[1])[0]
		#print "Quality="+quality
		print >>scanItem, "Quality:"+quality+','
		
		tmplist = re.split('Signal level=', cell)
		if len(tmplist) <= 1:
			continue
		
		signal_level = re.split(' ', tmplist[1])[0]
		print >>scanItem, "Signal level:"+signal_level+'}'
		i+=1
		
	print >>scanItem, "]}\n"
	
	#print scanItem.getvalue()
	
	f = open(wlanScanFile, 'a+')
	
	f.write(scanItem.getvalue())
	
	scanItem.close()
	
	f.close()
	
	exit()
